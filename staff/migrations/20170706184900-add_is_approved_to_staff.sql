-- +migrate Up
ALTER TABLE `staff` ADD COLUMN is_approved TINYINT(1) NOT NULL DEFAULT 0 AFTER `user_access_level`;