-- +migrate Up

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `mb_id` int(11) NOT NULL,
  `site_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `fname` varchar(128) NOT NULL DEFAULT '',
  `lname` varchar(128) NOT NULL DEFAULT '',
  `bio` text NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `image_url` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `multi_location` tinyint(1) NOT NULL,
  `appointment_trn` tinyint(1) NOT NULL,
  `reservation_trn` tinyint(1) NOT NULL,
  `independent_contractor` tinyint(1) NOT NULL,
  `always_allow_double_booking` tinyint(1) NOT NULL,
  `user_access_level` varchar(32) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`staff_id`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `staff_details` (
  `staff_details_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) unsigned NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `quote` text NOT NULL,
  PRIMARY KEY (`staff_details_id`),
  KEY `staff_id` (`staff_id`),
  CONSTRAINT `staff_details_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `staff_social_profile` (
  `staff_social_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) unsigned NOT NULL,
  `type` enum('facebook','instagram','google') NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`staff_social_profile_id`),
  KEY `staff_id` (`staff_id`),
  CONSTRAINT `staff_social_profile_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `staff_address` (
  `staff_address_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) unsigned NOT NULL,
  `address` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(64) NOT NULL DEFAULT '',
  `state` varchar(64) NOT NULL DEFAULT '',
  `country` varchar(64) NOT NULL DEFAULT '',
  `postal_code` varchar(16) NOT NULL DEFAULT '',
  `foreign_zip` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`staff_address_id`),
  KEY `fk_staff_id` (`staff_id`),
  CONSTRAINT `fk_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `staff_location` (
  `staff_location_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`staff_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `staff_phone_type` (
  `staff_phone_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`staff_phone_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `staff_phone_type` (`staff_phone_type_id`, `name`)
VALUES
	(1,'mobile'),
	(2,'work'),
	(3,'home');

CREATE TABLE IF NOT EXISTS `staff_phone` (
  `staff_phone_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) unsigned NOT NULL,
  `staff_phone_type_id` int(11) unsigned NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`staff_phone_id`),
  KEY `staff_id` (`staff_id`),
  KEY `staff_phone_type_id` (`staff_phone_type_id`),
  CONSTRAINT `staff_phone_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `staff_phone_ibfk_2` FOREIGN KEY (`staff_phone_type_id`) REFERENCES `staff_phone_type` (`staff_phone_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `staff_favourite` (
  `staff_favourite_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) unsigned NOT NULL,
  `account_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_favourite_id`),
  UNIQUE KEY `account_id` (`account_id`,`staff_id`),
  KEY `staff_id` (`staff_id`),
  CONSTRAINT `staff_favourite_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `staff_photo` (
  `staff_photo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `staff_id` int(11) unsigned NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `caption` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`staff_photo_id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `staff_id` (`staff_id`),
  CONSTRAINT `staff_photo_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;