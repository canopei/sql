-- +migrate Up
ALTER TABLE `staff_social_profile` CHANGE `type` `type` ENUM('facebook', 'instagram', 'twitter') NOT NULL;