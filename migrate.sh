#!/bin/sh

for db in account site staff class sale; do
    echo "Migrating '$db'"
    (cd $db && sql-migrate up)
    echo ""
done