-- +migrate Up
CREATE TABLE `booking` (
  `booking_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(11) unsigned NOT NULL,
  `account_id` int(11) unsigned NOT NULL,
  `site_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `account_membership_id` int(11) unsigned NOT NULL,
  `membership_service_id` int(11) unsigned NOT NULL,
  `service_id` int(11) unsigned NOT NULL,
  `status` varchar(32) NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`booking_id`),
  KEY `account_Id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;