-- +migrate Up
CREATE TABLE `city_group` (
  `city_group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`city_group_id`),
  UNIQUE KEY `name` (`name`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `city_group_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;