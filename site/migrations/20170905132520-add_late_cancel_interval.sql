-- +migrate Up
ALTER TABLE `location` ADD COLUMN `late_cancel_interval` SMALLINT(4) NOT NULL DEFAULT 60 AFTER `postal_code`;