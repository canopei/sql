-- +migrate Up
ALTER TABLE `site` ADD COLUMN is_approved TINYINT(1) NOT NULL DEFAULT 0 AFTER `sng_payment_method_mb_id`;
ALTER TABLE `location` ADD COLUMN is_approved TINYINT(1) NOT NULL DEFAULT 0 AFTER `longitude`;