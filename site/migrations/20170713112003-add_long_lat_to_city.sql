-- +migrate Up
ALTER TABLE `city` ADD COLUMN `latitude` DECIMAL(11,8) NOT NULL AFTER `sort_order`;
ALTER TABLE `city` ADD COLUMN `longitude` DECIMAL(11,8) NOT NULL AFTER `sort_order`;