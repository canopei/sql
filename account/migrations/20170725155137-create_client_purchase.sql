-- +migrate Up
ALTER TABLE `client` ADD COLUMN `purchases_synced_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `inactive`;
CREATE TABLE `client_purchase` (
  `client_purchase_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `sale_mb_id` int(11) unsigned NOT NULL,
  `client_id` int(11) unsigned NOT NULL,
  `site_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `description` text NOT NULL,
  `price` decimal(4,2) NOT NULL,
  `amount_paid` decimal(4,2) NOT NULL,
  `discount` decimal(4,2) DEFAULT NULL,
  `tax` decimal(4,2) NOT NULL,
  `returned` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` tinyint(3) NOT NULL,
  `sale_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_id` int(11) unsigned NOT NULL,
  `payment_type` varchar(64) NOT NULL DEFAULT '',
  `payment_method` int(11) NOT NULL,
  `payment_notes` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_purchase_id`),
  KEY `client_id` (`client_id`),
  KEY `uuid` (`uuid`),
  KEY `site_id` (`site_id`,`sale_mb_id`),
  CONSTRAINT `client_purchase_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;