-- +migrate Up
ALTER TABLE `client` ADD COLUMN `purchases_synced_at` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `inactive`;
