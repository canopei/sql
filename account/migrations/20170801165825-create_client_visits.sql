-- +migrate Up
CREATE TABLE `client_visit` (
  `client_visit_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `mb_id` int(11) unsigned NOT NULL,
  `client_id` int(11) unsigned NOT NULL,
  `site_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `staff_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `class_id` int(11) unsigned NOT NULL,
  `service_mb_id` int(11) unsigned NOT NULL,
  `service_count` tinyint(4) NOT NULL,
  `service_remaining` tinyint(4) NOT NULL,
  `late_cancelled` tinyint(1) NOT NULL,
  `make_up` tinyint(1) NOT NULL,
  `web_signup` tinyint(1) NOT NULL,
  `signed_in` tinyint(1) NOT NULL,
  `start_datetime` datetime NOT NULL,
  `end_datetime` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_visit_id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `client_id` (`client_id`),
  KEY `site_id` (`site_id`,`mb_id`),
  CONSTRAINT `client_visit_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;