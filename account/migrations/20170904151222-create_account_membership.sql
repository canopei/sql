-- +migrate Up
CREATE TABLE `account_membership` (
  `account_membership_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `membership_id` int(11) unsigned NOT NULL,
  `remaining` tinyint(4) NOT NULL DEFAULT '-1',
  `valid_until` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_membership_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `account_membership_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;